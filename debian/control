Source: libschedule-cron-events-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Lucas Kanashiro <kanashiro.duarte@gmail.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libset-crontab-perl <!nocheck>,
                     libtest-deep-perl <!nocheck>,
                     perl
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libschedule-cron-events-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libschedule-cron-events-perl.git
Homepage: https://metacpan.org/release/Schedule-Cron-Events

Package: libschedule-cron-events-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libset-crontab-perl
Description: module to find out the times at which a cron entry would run
 Given a line from a crontab, tells you the time at which cron will next run
 the line, or when the last event occurred, relative to any date you choose.
 The object keeps that reference date internally, and updates it when you call
 nextEvent() or previousEvent() - such that successive calls will give you a
 sequence of events going forward, or backwards, in time.
 .
 Use setCounterToNow() to reset this reference time to the current date on
 your system, or use setCounterToDate() to set the reference to any arbitrary
 time, or resetCounter() to take the object back to the date you constructed
 it with.
 .
 Schedule::Cron::Events uses Set::Crontab to understand the date
 specification, so it should be able to handle all forms of cron entries.
